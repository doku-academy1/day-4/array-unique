import java.util.*;
public class Main {
    public static void main(String[] args) {
        Integer[] arrays1 = {3,8};
        Integer[] arrays2 = {2,8};

        List<Integer> list1 = convertArrayToList(arrays1);
        List<Integer> list2 = convertArrayToList(arrays2);

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i<list1.size(); i++) {
            map.put(list1.get(i), 1);
        }

        for (int j = 0; j<list2.size(); j++) {
            if (map.containsKey(list2.get(j))) {
                map.remove(list2.get(j));
            }
        }
        List<Integer> finalArray = new ArrayList<>();
        for (Integer key: map.keySet()) {
            finalArray.add(key);
        }
        System.out.println(finalArray);

    }

    public static List<Integer> convertArrayToList(Integer[] arrays) {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i<arrays.length; i++) {
            arrayList.add(arrays[i]);
        }
        return arrayList;
    }
}